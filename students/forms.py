from django import forms

from phonenumber_field.formfields import PhoneNumberField

from students.models import Student


class CreateStudentForm(forms.ModelForm):
    phone_number = PhoneNumberField(required=True, region="UA", widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter UA phone number',
    }))

    # date_of_birth = forms.DateField(widget=AdminDateWidget())

    class Meta:
        model = Student
        fields = ['last_name', 'first_name', 'rating', 'phone_number', 'date_of_birth', 'group']
        widgets = {
            'date_of_birth': forms.DateInput(attrs={'class': 'date_picker'})
        }
