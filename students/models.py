import uuid as uuid

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from phonenumber_field.modelfields import PhoneNumberField


class Student(models.Model):
    first_name = models.CharField(max_length=150, null=False)
    last_name = models.CharField(max_length=150, null=False)
    date_of_birth = models.DateField(null=True)
    rating = models.PositiveSmallIntegerField(null=True, default=0,
                                              validators=[MaxValueValidator(100), MinValueValidator(1)])
    phone_number = PhoneNumberField(null=True, region="UA")
    uuid = models.UUIDField(auto_created=True, editable=False, default=uuid.uuid4)

    group = models.ForeignKey(
        to='groups.Group',
        on_delete=models.SET_NULL,
        null=True,
        related_name='group_of_student'
    )

    def __str__(self):
        return f'{self.first_name}  {self.last_name}'
