import random

from faker import Faker

from groups.models import Group

from students.models import Student


def generate_student():
    faker = Faker()
    groups = Group.objects.all()
    student = Student(
        first_name=faker.first_name(),
        last_name=faker.last_name(),
        date_of_birth=faker.date_of_birth(minimum_age=25, maximum_age=45),
        rating=random.randint(1, 100),
        group=random.choice(groups),
    )
    student.save()


def generate_students(request):
    count = parse_int(request, 'count')
    for _ in range(count):
        generate_student()
    return "{} of students generated successfully!".format(count)


def parse_int(request, param_name, min_limit=0, max_limit=100, default=None):
    value = request.GET.get(param_name, default)

    if not is_integer(value):
        raise ValueError(f"Type error for param {param_name}:"
                         f" not integer value")

    value_int = int(value)

    if not min_limit < value_int < max_limit:
        raise ValueError(f"Range error for param {param_name}: "
                         f"out of the range [{min_limit}..{max_limit}]")

    return value_int


def is_integer(n):
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()


def get_all_students():
    return Student.objects.all()
