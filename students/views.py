from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse

from students import utils
from students.forms import CreateStudentForm
from students.models import Student


def generate_students(request):
    return HttpResponse(utils.generate_students(request))


def view_all_students(request):
    all_students = utils.get_all_students()
    if request.method == 'GET':
        params = [
            'first_name',
            'last_name',
            'rating',
        ]
        for param in params:
            value = request.GET.get(param)
            if value:
                all_students = all_students.filter(**{param: value})

    return render(
        request=request,
        context={
            'all_students': all_students,
        },
        template_name='view-all-students.html'
    )


def students_create(request):
    if request.method == 'GET':
        form = CreateStudentForm()

    elif request.method == "POST":
        form = CreateStudentForm(
            data=request.POST,
        )

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:list_all"))

    return render(
        request=request,
        context={
            "form": form
        },
        template_name="students-edit.html"

    )


def edit_student(request, student_id):
    try:
        student = Student.objects.get(uuid=student_id)
    except Student.DoesNotExist:
        return Http404("Student DoesNotExist")

    if request.method == "POST":
        form = CreateStudentForm(
            instance=student,
            data=request.POST
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:list_all"))

    else:
        form = CreateStudentForm(
            instance=student,
        )

    return render(
        request=request,
        context={
            "form": form,
        },
        template_name="students-edit.html",
    )


def remove_student(request, student_id):
    student = get_object_or_404(Student, uuid=student_id)
    if request.method == 'POST':
        student.delete()
        return redirect(reverse('students:list_all'))
    return render(
        request=request,
        context={
            "student": student,
            "what_to_delete": str(student)
        },
        template_name='student-delete.html',
    )
