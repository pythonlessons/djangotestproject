from django.urls import path

from students.views import edit_student, generate_students, remove_student, students_create, view_all_students

app_name = 'students'

urlpatterns = [
    path('list/', view_all_students, name='list_all'),
    path('generate/', generate_students, name='generate'),
    path('create/', students_create, name='create'),
    path('edit/<str:student_id>', edit_student, name='edit'),
    path('remove/<str:student_id>', remove_student, name='remove')
]
