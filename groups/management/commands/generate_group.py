import random
import string

from django.core.management import BaseCommand

from groups.models import Group

from teachers.models import Teacher


class Command(BaseCommand):
    def handle(self, *args, **options):
        if options['count']:
            from faker import Faker
            faker = Faker()
            faker.phone_number(),
            count = options['count']
            for _ in range(count):
                group_name = 'Group_' + ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))
                group_description = 'Group_description' \
                                    + ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(16))
                teacher = random.choice(Teacher.objects.all())
                group = Group(
                    group_name=group_name,
                    description=group_description,
                    teacher=teacher,
                )
                group.save()

    def add_arguments(self, parser):
        parser.add_argument(
            'count',
            type=int,
            help='Indicates the number of groups to be created'
        )
