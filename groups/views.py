from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
# Create your views here.
from django.urls import reverse

from groups.forms import GroupsForm
from groups.models import Group


def create_group(request):
    if request.method == 'GET':
        return render(
            request=request,
            context={
                "form": GroupsForm()
            },
            template_name="group-create.html"
        )
    if request.method == "POST":
        form = GroupsForm(
            data=request.POST
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('groups:list_all'))


def view_all_groups(request):
    return render(
        request=request,
        context={
            "all_groups": Group.objects.all()
        },
        template_name="view-all-group.html"
    )


def group_edit(request, group_id):
    try:
        group = Group.objects.get(uuid=group_id)
        form = GroupsForm(instance=group, group_uuid=group.id)
    except Group.DoesNotExist:
        return Http404("Student DoesNotExist")

    if request.method == "POST":
        form = GroupsForm(
            instance=group,
            data=request.POST,
            group_uuid=group.id
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("groups:list_all"))
    return render(
        request=request,
        context={
            "form": form,
            "group": group,
        },
        template_name="group-edit.html"
    )


def remove_group(request, group_id):
    group = get_object_or_404(Group, uuid=group_id)
    if request.method == 'POST':
        group.delete()
        return redirect(reverse('groups:list_all'))
    return render(
        request=request,
        context={
            "group": group,
            "what_to_delete": str(group),
        },
        template_name='group-delete.html',
    )
