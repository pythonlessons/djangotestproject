from django.urls import path

from groups.views import create_group, group_edit, remove_group, view_all_groups

app_name = 'groups'

urlpatterns = [
    path('list/', view_all_groups, name='list_all'),
    path('create/', create_group, name="create"),
    path('edit/<uuid:group_id>', group_edit, name='edit'),
    path('remove/<uuid:group_id>', remove_group, name='remove')
]
