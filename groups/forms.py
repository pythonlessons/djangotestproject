from django import forms

from groups.models import Group

from students.models import Student


class GroupsForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['group_name', 'description', 'teacher', 'headman']

    def __init__(self, group_uuid, *args, **kwargs):
        super(GroupsForm, self).__init__(*args, **kwargs)
        self.fields['headman'].queryset = Student.objects.filter(group__id=group_uuid)
