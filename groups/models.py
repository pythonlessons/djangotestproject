import uuid as uuid

from django.db import models


class Group(models.Model):
    group_name = models.CharField(max_length=150, null=False)
    description = models.CharField(max_length=256, null=True)
    uuid = models.UUIDField(auto_created=True, editable=False, default=uuid.uuid4)
    teacher = models.ForeignKey(
        'teachers.Teacher',
        on_delete=models.SET_NULL,
        null=True,
        related_name='+',
    )

    headman = models.OneToOneField(
        to='students.Student',
        on_delete=models.SET_NULL,
        null=True,
        related_name="head_of_group"
    )

    def __str__(self):
        return f'{self.group_name}'
