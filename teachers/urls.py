from django.urls import path

from teachers.views import teacher_create, teacher_edit, teacher_remove, teachers_list

app_name = 'teachers'

urlpatterns = [
    path('list/', teachers_list, name='list_all'),
    path('create/', teacher_create, name='create'),
    path('edit/<uuid:teacher_uuid>', teacher_edit, name='edit'),
    path('remove/<uuid:teacher_uuid>', teacher_remove, name='remove'),
]
