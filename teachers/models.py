import datetime
import uuid

from django.db import models


# Create your models here.


class Teacher(models.Model):
    first_name = models.CharField(max_length=150, null=False)
    last_name = models.CharField(max_length=150, null=False)
    date_of_birth = models.DateField(default=datetime.datetime.now(), null=True)
    uuid = models.UUIDField(auto_created=True, editable=False, default=uuid.uuid4)

    def __str__(self):
        return f'{self.first_name}  {self.last_name}'
