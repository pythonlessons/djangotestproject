from django import forms

from teachers.models import Teacher


class TeachersForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = ['first_name', 'last_name', 'date_of_birth']
