from django.core.management import BaseCommand

from teachers.models import Teacher


class Command(BaseCommand):
    def handle(self, *args, **options):
        if options['count']:
            from faker import Faker
            faker = Faker()
            faker.phone_number(),
            count = options['count']
            for _ in range(count):
                teacher = Teacher(first_name=faker.first_name(),
                                  last_name=faker.last_name(),
                                  date_of_birth=faker.date_of_birth(minimum_age=25, maximum_age=100)
                                  )
                teacher.save()

    def add_arguments(self, parser):
        parser.add_argument(
            'count',
            type=int,
            help='Indicates the number of teachers to be created'
        )
