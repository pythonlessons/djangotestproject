from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse

from groups.models import Group

from teachers.forms import TeachersForm
from teachers.models import Teacher


def teachers_list(request):
    return render(
        context={
            'all_teachers': Teacher.objects.all(),
        },
        request=request,
        template_name='view-all-teachers.html',
    )


def teacher_create(request):
    if request.method == 'POST':
        form = TeachersForm(
            data=request.POST
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:list_all"))
    if request.method == "GET":
        return render(
            request=request,
            context={
                "form": TeachersForm(),
            },
            template_name='teacher-create.html',
        )


def teacher_edit(request, teacher_uuid):
    try:
        teacher = Teacher.objects.get(uuid=teacher_uuid)
        groups = Group.objects.all().filter(teacher__uuid=teacher.uuid)
        form = TeachersForm(instance=teacher)
    except Teacher.DoesNotExist:
        return Http404("Student DoesNotExist")

    if request.method == "POST":
        form = TeachersForm(
            instance=teacher,
            data=request.POST,
        )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("teachers:list_all"))

    return render(
        request=request,
        context={
            "form": form,
            "groups": groups,
        },
        template_name="teacher-edit.html"
    )


def teacher_remove(request, teacher_uuid):
    teacher = get_object_or_404(Teacher, uuid=teacher_uuid)
    if request.method == 'POST':
        teacher.delete()
        return redirect(reverse('teachers:list_all'))
    return render(
        request=request,
        context={
            "what_to_delete": str(teacher),
            "url_to_redirect": "{% url 'teachers:list_all' %}"
        },
        template_name='teacher-delete.html',
    )
