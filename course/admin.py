from course.models import Course, Technology

from django.contrib import admin


class TechnologyAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    fields = ['technology_name', 'description', 'uuid']
    readonly_fields = ['uuid']
    model = Technology


class CourseAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    readonly_fields = ['uuid']
    fields = ['course_name', 'description', 'technology', 'uuid']
    model = Course


admin.site.register(Course, CourseAdmin)
admin.site.register(Technology, TechnologyAdmin)
