import uuid as uuid

from django.db import models


# Create your models here.
class Technology(models.Model):
    technology_name = models.CharField(max_length=150, null=False)
    description = models.CharField(max_length=256, null=True)
    uuid = models.UUIDField(auto_created=True, editable=False, default=uuid.uuid4)

    def __str__(self):
        return self.technology_name


class Course(models.Model):
    course_name = models.CharField(max_length=150, null=False)
    description = models.CharField(max_length=256, null=True)
    uuid = models.UUIDField(auto_created=True, editable=False, default=uuid.uuid4)
    technology = models.ManyToManyField(
        to=Technology,
        null=True,
    )

    def __str__(self):
        return self.course_name
