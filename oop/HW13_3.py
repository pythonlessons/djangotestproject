class Robot:

    def __init__(self, color=None, cpu=None, camerasCount=2):
        self.color = color
        self.cpu = cpu
        self.camerasCount = camerasCount

    def print_parameters(self):
        print(self)

    def __str__(self):
        return f'{self.color}  {self.cpu} {self.camerasCount}'


class SpotMini(Robot):

    def __init__(self, color=None, cpu=None, camerasCount=2, legs_count=4):
        super().__init__(color, cpu, camerasCount)
        self.legs_count = legs_count

    def legs_count(self):
        return self.legs_count

    def __str__(self):
        return super().__str__() + f'{self.legs_count}'


class Atlas(Robot):

    def __init__(self, color=None, cpu=None, camerasCount=2, legs_count=2, arms=2):
        super().__init__(color, cpu, camerasCount)
        self.arms = arms
        self.legs_count = legs_count

    def __str__(self):
        return super().__str__() + f'{self.legs_count} {self.arms}'

    def get_arms(self):
        return self.arms
