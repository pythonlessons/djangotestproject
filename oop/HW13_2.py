class Shape:
    pass


class Point(Shape):

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class Circle():

    def __init__(self, xC=0, yC=0, radius=0):
        self.xC = xC
        self.yC = yC
        self.radius = radius

    def aplyToCircle(self, point):
        return ((point.x - self.xC) ** 2 + (point.y - self.yC) ** 2 <= self.radius * self.radius)


c = Circle(radius=5)
print(c.aplyToCircle(Point(1, 1)))
